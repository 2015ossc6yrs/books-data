#!/usr/bin/ruby

require 'rakuten_web_service'
require 'mongo'

RakutenWebService.configuration do |c|
  c.application_id = '1010355418644878299'
  c.affiliate_id = '142452b2.e02c6c39.142452b3.db12a911'
end

client = Mongo::Client.new([ '127.0.0.1:27017' ], :database => 'bookstore')

books = RakutenWebService::Books::Book.search(:booksGenreId => '001005')
books.each do |item|
  #puts item['publisherName']
  client[:books].insert_one({title: item['title'], author: item['author'], 
  publisherName: item['publisherName'], size: item['size'], isbn: item['isbn'],
  price: item['itemPrice'], itemUrl: item['itemUrl'], imageUrl: item['largeImageUrl']})
  sleep 1
end
