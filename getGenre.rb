#!/usr/bin/ruby

require 'rakuten_web_service'
require 'mongo'

RakutenWebService.configuration do |c|
  c.application_id = '1010355418644878299'
  c.affiliate_id = '142452b2.e02c6c39.142452b3.db12a911'
end

client = Mongo::Client.new([ '127.0.0.1:27017' ], :database => 'bookstore')

def get_genre(genre_id, client)
 genre = RakutenWebService::Books::Genre[genre_id]
 #puts genre.id, genre.name
 sleep 1
 genre.children.each do |child|
  puts child.id, child.name, genre_id
  client[:genre].insert_one({'id' => child.id, 'name' => child.name, 'parent' => genre_id})
  get_genre(child.id, client)
 end
end

get_genre('001', client)
